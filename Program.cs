﻿using Npgsql;
using System;
using System.IO;

namespace Extrato
{
    class Program
    {
        static void Main(string[] args)
        {
            var cs = "Host=localhost;Username=postgres;Password=postgres;Database=postgres";
            string nomeArquivo = LeNomeArquivo();
            try
            {
                using var con = new NpgsqlConnection(cs);
                con.Open();
                using var cmd = new NpgsqlCommand();
                cmd.Connection = con;

                string[] lines = File.ReadAllLines(nomeArquivo);

                for (int i = 1; i < lines.Length; i++)
                {
                    string[] colunas = lines[i].Split(new char[] { ' ', '\t', '\n', '\r', ';' }, StringSplitOptions.RemoveEmptyEntries);
                    string comando = CriaInsert(colunas);
                    if(comando == null)
                    {
                        Console.WriteLine("Erro ao inserir registro ", i, "pois existe CPF ou CNPJ inválido.");
                        continue;
                    }
                    cmd.CommandText = comando;
                    cmd.ExecuteNonQuery();
                }
            }catch(Exception e)
            {
                throw new Exception("Ocorreu um erro!", e);
            }
            
        }

        private static string LimpaCpfCnpj(string cpf_cnpj)
        {
            string cpfCnpjFormatado = String.Empty;
            foreach(var character in cpf_cnpj)
            {
                if (char.IsNumber(character))
                    cpfCnpjFormatado += character;
            }
            return cpfCnpjFormatado;
        }

        private static string CriaInsert(string[] registro)
        {
            bool valido = true;
            string cpf = LimpaCpfCnpj(registro[0]);
            if(cpf.Length == 11)
            {
                valido = CpfValido(cpf);
            }else if(cpf.Length == 14)
            {
                valido = CnpjValido(cpf);
            }else { valido = false; }
            bool priv = registro[1] == "1" ? true : false;
            bool incompleto = registro[2] == "1" ? true : false;

            string comando = "INSERT INTO extrato (cpf_cnpj, private, incompleto, dt_ultima_compra, ticket_medio, ticket_ultima_compra, lj_frequente, lj_ultima_compra) VALUES(" + "'" + cpf + "'," + priv + "," + incompleto;

            if (registro[3].Equals("NULL"))
            {
                comando += ", null";
            }
            else
            {
                comando += ", '" + registro[3] + "'";
            }

            if (registro[4].Equals("NULL"))
            {
                comando += ", null";
            }
            else
            {
                comando  += "," + registro[4].Replace(",",".");
            }

            if (registro[5].Equals("NULL"))
            {
                comando += ", null";
            }
            else
            {
                comando += "," + registro[5].Replace(",", ".");
            }

            if (registro[6].Equals("NULL"))
            {
                comando += ", null";
            }
            else
            {
                comando += ", '" + LimpaCpfCnpj(registro[6]) + "'";
            }

            if (registro[7].Equals("NULL"))
            {
                comando += ", null";
            }
            else
            {
                comando += ", '" + LimpaCpfCnpj(registro[7]) + "'";
            }

            comando += ")";
            return valido ? comando : null;
        }

        public static string LeNomeArquivo()
        {
            Console.WriteLine("Nome do arquivo:");
            string nomeArquivo = Console.ReadLine();
            Console.WriteLine("Inserindo registros do arquivo", nomeArquivo, "no banco...");
            return nomeArquivo;
        }

        public static bool CnpjValido(string cnpj)
        {
            int[] multiplicador1 = new int[12] { 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplicador2 = new int[13] { 6, 5, 4, 3, 2, 9, 8, 7, 6, 5, 4, 3, 2 };
            int soma;
            int resto;
            string digito;
            string tempCnpj;
            
            tempCnpj = cnpj.Substring(0, 12);
            soma = 0;
            for (int i = 0; i < 12; i++)
                soma += int.Parse(tempCnpj[i].ToString()) * multiplicador1[i];
            resto = (soma % 11);
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = resto.ToString();

            tempCnpj = tempCnpj + digito;
            soma = 0;
            for (int i = 0; i < 13; i++)
                soma += int.Parse(tempCnpj[i].ToString()) * multiplicador2[i];
            resto = (soma % 11);
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = digito + resto.ToString();
            return cnpj.EndsWith(digito);
        }

        public static bool CpfValido(string cpf)
        {
            int[] multiplicador1 = new int[9] { 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            int[] multiplicador2 = new int[10] { 11, 10, 9, 8, 7, 6, 5, 4, 3, 2 };
            string tempCpf;
            string digito;
            int soma;
            int resto;

            tempCpf = cpf.Substring(0, 9);
            soma = 0;

            for (int i = 0; i < 9; i++)
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador1[i];
            resto = soma % 11;
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = resto.ToString();
            tempCpf = tempCpf + digito;
            soma = 0;
            for (int i = 0; i < 10; i++)
                soma += int.Parse(tempCpf[i].ToString()) * multiplicador2[i];
            resto = soma % 11;
            if (resto < 2)
                resto = 0;
            else
                resto = 11 - resto;
            digito = digito + resto.ToString();
            return cpf.EndsWith(digito);
        }

    }
}
