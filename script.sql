CREATE TABLE IF NOT EXISTS public.extrato
(
    cpf_cnpj character varying(14) COLLATE pg_catalog."default" NOT NULL,
    private boolean DEFAULT false,
    incompleto boolean DEFAULT false,
    dt_ultima_compra date,
    ticket_medio numeric,
    ticket_ultima_compra numeric,
    lj_frequente character varying(14) COLLATE pg_catalog."default",
    lj_ultima_compra character varying(14) COLLATE pg_catalog."default",
    CONSTRAINT extrato_pkey PRIMARY KEY (cpf_cnpj)
)
WITH (
    OIDS = FALSE
)
TABLESPACE pg_default;

ALTER TABLE public.extrato
    OWNER to postgres;